package no.jonkjenn.kalkulator;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Locale;

import android.widget.TextView;

public class Brain {
	TextView display;
	TextView prevDisplay;
	HashMap<Integer, CalcButton> buttonMap;
	Boolean alternate = false;
	String displayString = "";
	String bufferString = "";
	Operation currentOperation;
	Operation nextOperation;
	
	
	public Brain(TextView display,TextView prevDisplay,HashMap<Integer,CalcButton> buttonMap)
	{
		this.display = display; 
		this.buttonMap = buttonMap;
		this.prevDisplay = prevDisplay;
		
	}
	
	public void buttonClick(int id)
	{
		CalcButton b = buttonMap.get(id);
		
		if(b==null){
			return;
		}
		else
		{
			parseButton(b);
		}
	}
	
	private void parseButton(CalcButton b)
	{
		if(this.alternate)//Alternate button pressed, shift
		{
			
		}
		else
		{
			if(b.input != null)
			{
				handleString(b.input);
			}
			else if(b.operation == Operation.EQUALS)
			{
				ShuntingYard sy = new ShuntingYard();
				String s = sy.StringToReversePolish(displayString);
				ReversePolish rpn = new ReversePolish();
				double res = rpn.calculateRPN(s);
				DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.getDefault());
				otherSymbols.setDecimalSeparator('.');
				DecimalFormat df = new DecimalFormat("#.######",otherSymbols);
				//Log.v("test", "Result: " + df.format(res));
				prevDisplay.setText(displayString);
				clearAndSetDisplay(df.format(res));
			}
			else if(b.operation == Operation.DEL)
			{
				delButton();
			}
			else if(b.operation == Operation.AC)
			{
				clearAndSetDisplay("");
			}
		}
		
	}
	
	private void delButton()
	{
		if(displayString.length()>0)
		{
			displayString = displayString.substring(0, displayString.length()-1 );
			clearAndSetDisplay(displayString);
		}
	}
	
	private void setDisplay(String s)
	{
		display.setText(s);
	}
	
	private void setDisplay(BigDecimal bd)
	{
		display.setText(bd.toString());
		bufferString = "";
	}
	
	private void handleValue(BigDecimal value)
	{
		
	}
	
	
	private void handleString(String s)
	{
			if(displayString.equals("0") && !s.equals("."))
			{
				displayString = "";
			}
			else if(s.equals("."))//TODO: Add check for multiple . in same number
			{
				String lastChar = displayString.substring(displayString.length()-1,displayString.length());
				try
				{
					Integer.parseInt(lastChar);
				}
				catch(Exception e)
				{
					displayString = displayString + "0";
				}
			}
			
			displayString = displayString + s;
			display.setText(displayString);
	}
	
	private void clearAndSetDisplay(String s)
	{
			if(s.equals("") || s.equals(" "))
			{
				s = "0";
			}
			
			displayString = s;
			display.setText(displayString);
	}
	
	
	
	public void inputChar(char c)
	{
		
	}

}
