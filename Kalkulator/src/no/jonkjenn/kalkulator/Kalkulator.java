package no.jonkjenn.kalkulator;

import java.util.HashMap;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class Kalkulator extends Activity {
	Brain brain;
		
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        TextView forrige_resultat = (TextView)findViewById(R.id.forrige_resultat);
        
        brain = new Brain((TextView)findViewById(R.id.resultat),forrige_resultat,LoadButtons());
    }
    
    private HashMap<Integer, CalcButton> LoadButtons()
    {
    	HashMap<Integer, CalcButton> map = new HashMap<Integer, CalcButton>();
    	
    	map.put(0,new CalcButton(0, "0"));
    	map.put(1, new CalcButton(1,"-")); 
    	map.put(2, new CalcButton(2, "."));
    	//TODO Add EXP, exponential numbers
    	map.put(3, new CalcButton(3, "0"));
    	map.put(4,new CalcButton(4, Operation.EQUALS));
    	map.put(5, new CalcButton(5, "1"));
    	map.put(6, new CalcButton(6, "2"));
    	map.put(7, new CalcButton(7, "3"));
    	map.put(8,new CalcButton(8, "+"));
    	map.put(9,new CalcButton(9, "-"));
    	map.put(10, new CalcButton(10, "4"));
    	map.put(11, new CalcButton(11, "5"));
    	map.put(12, new CalcButton(12, "6"));
    	map.put(13, new CalcButton(13, "*"));
    	map.put(14, new CalcButton(14, "/"));
    	map.put(15, new CalcButton(15, "7"));
    	map.put(16, new CalcButton(16, "8"));
    	map.put(17, new CalcButton(17, "9"));
    	map.put(18, new CalcButton(18, Operation.DEL));
    	map.put(19, new CalcButton(19, Operation.AC));
    	map.put(20, new CalcButton(20, ")"));
    	map.put(21, new CalcButton(21, "("));
    	map.put(22, new CalcButton(22, "sqrt("));
    	map.put(23, new CalcButton(23, "^"));
    	map.put(24, new CalcButton(24, "cos("));
    	map.put(25, new CalcButton(25, "sin("));
    	map.put(26, new CalcButton(26, "tan("));
    	
    	return map;
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    public void knapp(View view) 
    {
    	Button knappen = (Button)view;
    	Object tag = knappen.getTag();
    	if(tag != null)
    	{
    		int id = Integer.parseInt(tag.toString()); 
    		brain.buttonClick(id);
    	}
    }
    
    public void del(View view)
    {
    	
    	//if(t.length()>0)
    	{
			//v.setText(t.substring(0, t.length()-1));
    	}
    }
}
    	
    	
    	
