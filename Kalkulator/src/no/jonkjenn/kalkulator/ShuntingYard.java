package no.jonkjenn.kalkulator;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;
import android.util.Log;

public class ShuntingYard {
	Map<String,Operator> operators;
	
	public ShuntingYard()
	{
		loadOperators();
	}
	
	private void loadOperators()
	{
		operators = new HashMap<String,Operator>(); 
		operators.put("+", new Operator("+", 2, Associativity.LEFT));		
		operators.put("-", new Operator("-", 2, Associativity.LEFT));		
		operators.put("*", new Operator("*", 3, Associativity.LEFT));		
		operators.put("/", new Operator("/", 3, Associativity.LEFT));		
		operators.put("^", new Operator("^", 4, Associativity.RIGHT));		
		operators.put("sin", new Operator("sin",4,Associativity.RIGHT));
		operators.put("cos", new Operator("cos",4,Associativity.RIGHT));
		operators.put("tan", new Operator("tan",4,Associativity.RIGHT));
		operators.put("sqrt", new Operator("sqrt",4,Associativity.RIGHT));
	}
	
	public String StringToReversePolish(String s){
		Stack<String> operatorStack = new Stack<String>();
		Queue<String> outputQueue = new LinkedList<String>();		
		
		for(int i=0;i<s.length();i++)
		{
			String c = s.substring(i, i+1);
			//Log.v("test2", c + " test2");
			if(c.equals(" "))
			{
				continue;
			}
			else if(c.matches("[0-9]"))
			{
				int y = checkForLargerNumber(s,i);
				outputQueue.add(s.substring(i,y+1));
				i= y;
			}
			else if(c.equals("("))
			{
				operatorStack.push(c);
			}
			else if(c.equals(")"))
			{
				while(!operatorStack.peek().equals("("))
				{
					if(operatorStack.size()==1)
					{
						break;
						//TODO ERROR WRONG PARENTHESIS
					}
					outputQueue.add(operatorStack.pop());
				}
				
				operatorStack.pop();
			}
			else if(c.equals("s"))
			{
				String c2 = s.substring(i+1, i+2);//For finding sqrt instead of sin
				if(c2.equals("i"))
				{
					operatorStack.push("sin");
					i= i+2;
				}
				else if(c2.equals("q"))
				{
					operatorStack.push("sqrt");
					i= i+3;
				}
			}
			else if(c.equals("c"))
			{
				operatorStack.push("cos");
				i= i+2;
			}
			else if(c.equals("t"))
			{
				operatorStack.push("tan");
				i= i+2;
			}
			else if(operators.containsKey(c))
			{
				if(operatorStack.size()>0)
				{
					String stackPeek = operatorStack.peek();
					if(stackPeek.equals("(")){ operatorStack.push(c); continue;}
					Operator stackOp = operators.get(stackPeek);
					Operator thisOp = operators.get(c);
					
					while((thisOp.associativity == Associativity.LEFT && (thisOp.presedence <= stackOp.presedence)) || (thisOp.presedence < stackOp.presedence) || (thisOp.presedence == stackOp.presedence && thisOp.associativity == stackOp.associativity))
							{
								if(stackPeek.equals("(")){ continue;}
								outputQueue.add(operatorStack.pop());
								if(operatorStack.size()==0){break;}
								stackPeek = operatorStack.peek();
								stackOp = operators.get(stackPeek);
								if(stackOp == null){break;}
								continue;
							}
							operatorStack.push(c);
				}
				else
				{
					operatorStack.push(c);
					continue;
				}
			}
			
		}
		
		while(operatorStack.size()>0)
		{
			outputQueue.add(operatorStack.pop());
		}
	
		String str = "";
		
		while(outputQueue.size()>0)
		{
			str = str + " " + outputQueue.remove();
		}
	/*	String str2 = "";
		while(operatorStack.size()>0)
		{
			//str2 = str2 + " " + operatorStack.pop();
		}*/
		
		Log.v("test","Test: " + str);// + " - " + str2);
		
		return str;		
	}

	private int checkForLargerNumber(String s, int i) {
		for(;i<s.length();i++)
		{
			if(!s.substring(i,i+1).matches("[.0-9]"))
			{
				return i-1;				
			}
			else if(i == s.length()-1)
			{
				return i;
			}
		}
		
		return 1;
	}

}
