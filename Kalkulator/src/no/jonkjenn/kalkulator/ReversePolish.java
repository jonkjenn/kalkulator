package no.jonkjenn.kalkulator;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import android.util.Log;

public class ReversePolish {
	Map<String,Operator> operators;
	
	public ReversePolish()
	{
		operators = new HashMap<String,Operator>(); 
		operators.put("+", new Operator("+", 2, Associativity.LEFT));		
		operators.put("-", new Operator("-", 2, Associativity.LEFT));		
		operators.put("*", new Operator("*", 3, Associativity.LEFT));		
		operators.put("/", new Operator("/", 3, Associativity.LEFT));		
		operators.put("^", new Operator("^", 4, Associativity.RIGHT));
		operators.put("sin", new Operator("sin",4,Associativity.RIGHT));
		operators.put("cos", new Operator("cos",4,Associativity.RIGHT));
		operators.put("tan", new Operator("tan",4,Associativity.RIGHT));
		operators.put("sqr", new Operator("sqr",4,Associativity.RIGHT));
	}
	
	public double calculateRPN(String s)
	{
		double result;
		
		Stack<String> stack = new Stack<String>();
		
		String[] tokens = s.split(" ");
		
		for(int i=1;i<tokens.length;i++)
		{
			try
			{
				Double.parseDouble(tokens[i]);				
				stack.push(tokens[i]);				
				continue;
			}
			catch(Exception e)
			{
				try
				{
					Integer.parseInt(tokens[i]);
					stack.push(tokens[i]);
					continue;
				}
				catch(Exception ee)
				{
					
				}
			}
			
			if(tokens[i].equals("sin"))
			{
				double x = Double.parseDouble(stack.pop());				
				double res = Math.sin(x);
				stack.push(Double.toString(res));
			}
			else if(tokens[i].equals("cos"))
			{
				double x = Double.parseDouble(stack.pop());				
				double res = Math.cos(x);
				stack.push(Double.toString(res));
			}
			else if(tokens[i].equals("tan"))
			{
				double x = Double.parseDouble(stack.pop());				
				double res = Math.tan(x);
				stack.push(Double.toString(res));
			}
			else if(tokens[i].equals("sqrt"))
			{
				double x = Double.parseDouble(stack.pop());				
				double res = Math.sqrt(x);
				stack.push(Double.toString(res));
			}
			else if(operators.containsKey(tokens[i]) && stack.size()>1)
			{
				double x = Double.parseDouble(stack.pop());				
				double y = Double.parseDouble(stack.pop());				
				
				if(tokens[i].equals("+"))
				{
					double res = y + x;
					stack.push(Double.toString(res));
				}
				else if(tokens[i].equals("-"))
				{
					double res = y - x;
					stack.push(Double.toString(res));
				}
				else if(tokens[i].equals("*"))
				{
					double res = x * y;
					stack.push(Double.toString(res));
				}
				else if(tokens[i].equals("/"))
				{
					double res = y / x;
					stack.push(Double.toString(res));
				}
				else if(tokens[i].equals("^"))
				{
					double res = Math.pow(y, x);
					stack.push(Double.toString(res));
				}
			}
		}
		
		return Double.parseDouble(stack.pop());
	}

}
