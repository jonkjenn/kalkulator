package no.jonkjenn.kalkulator;

public class Operator {
	public String operator;
	public int presedence;
	public Associativity associativity;
	
	public Operator(String operator, int presedence, Associativity associativity)
	{
		this.operator = operator;
		this.presedence = presedence;
		this.associativity = associativity;
	}
	
}
