package no.jonkjenn.kalkulator;

import java.math.BigDecimal;

public class CalcButton {
	private int id;
	public Operation operation;
	public Operation altOperation;
	public String input;
	public String altinput;
	public BigDecimal value;
	public BigDecimal altvalue;
	
	public CalcButton(int id,Operation operation)	
	{
		this.id = id;
		this.operation = operation;
	}
	
	public CalcButton(int id, String input)
	{
		this.id = id;
		this.input = input;
	}
	
	public CalcButton(int id, BigDecimal value)
	{
		this.id = id;
		this.value = value;
	}
	
	public CalcButton(int id, Operation operation, Operation altOperation)
	{
		
	}

}
